# Arduino ci module
This module helps you get started with Arduino projects. It provides the necessary
libraries and helps you run builds and tests in a CI Pipeline.

## Installation

### Through package

#### Using apt-get (or aptitude)

If you're using FreeBSD, Debian, Raspbian or Ubuntu, you can find this in the `arduino-mk`
package which can be installed using `apt-get` or `aptitude`.

```sh
sudo apt-get install arduino-mk
```

#### homebrew (or linuxbrew)

If you're using homebrew (or [linuxbrew](https://github.com/Homebrew/linuxbrew)) then you can find this in the
`arduino-mk` package which can be installed using the following commands.

Also make sure you have the necessary dependencies installed. Refer to the [Requirements](#requirements) section below to install the dependencies.

```sh
# add tap
$ brew tap sudar/arduino-mk

# to install the last stable release
$ brew install arduino-mk

# to install the development version
$ brew install --HEAD arduino-mk
```
## Usage

Sample makefiles are provided in the `examples/` directory.  E.g. [Makefile-example](examples/MakefileExample/Makefile-example.mk) demonstrates some of the more advanced options,
whilst [Blink](examples/Blink/Makefile) demonstrates the minimal settings required for various boards like the Uno, Nano, Mega, Teensy, ATtiny etc.

### Mac

On the Mac with IDE 1.0 you might want to set:

```make
    ARDUINO_DIR   = /Applications/Arduino.app/Contents/Resources/Java
    ARDMK_DIR     = /usr/local
    AVR_TOOLS_DIR = /usr
    MONITOR_PORT  = /dev/ttyACM0
    BOARD_TAG     = mega2560
```

On the Mac with IDE 1.5+ it's like above but with

```make
    ARDUINO_DIR   = /Applications/Arduino.app/Contents/Java
```
### Linux

You can either declare following variables in your project's makefile or set them as environmental variables.

```make
    ARDUINO_DIR – Directory where Arduino is installed
    ARDMK_DIR – Directory where you have copied the makefile
    AVR_TOOLS_DIR – Directory where avr tools are installed
```

Keep in mind, that Arduino 1.5.x+ comes with it's own copy of avr tools which you can leverage in your build process here.

Example of  ~/.bashrc file:

```make
    export ARDUINO_DIR=/home/sudar/apps/arduino-1.0.5
    export ARDMK_DIR=/home/sudar/Dropbox/code/Arduino-Makefile
    export AVR_TOOLS_DIR=/usr/include
```

Example of the project's make file:

```make
    BOARD_TAG     = mega2560
    MONITOR_PORT  = /dev/ttyACM0
```

### Useful Variables

The list of all variables that can be overridden is available at [arduino-mk-vars.md](arduino-mk-vars.md) file.

- `BOARD_TAG` - Type of board, for a list see boards.txt or `make show_boards`
- `MONITOR_PORT` - The port where your Arduino is plugged in, usually `/dev/ttyACM0` or `/dev/ttyUSB0` in Linux or Mac OS X and `com3`, `com4`, etc. in Windows.
- `ARDUINO_DIR` - Path to Arduino installation. Using Windows with Cygwin,
  this path must use Unix / and not Windows \\ (eg "C:/Arduino" not
  "C:\\Arduino).
- `ARDMK_DIR`   - Path where the `*.mk` are present. If you installed the package, then it is usually `/usr/share/arduino`. On Windows, this should be a path without spaces and no special characters, it can be a *cygdrive* path if necessary and must use / not \\.
- `AVR_TOOLS_DIR` - Path where the avr tools chain binaries are present. If you are going to use the binaries that came with Arduino installation, then you don't have to set it. Otherwise set it relative and not absolute.

## Including Libraries

You can specify space separated list of libraries that are needed for your sketch in the variable `ARDUINO_LIBS`.

```make
	ARDUINO_LIBS = Wire SoftwareSerial
```

The libraries will be searched for in the following places in the following order.

- `/libraries` directory inside your sketchbook directory. Sketchbook directory will be auto detected from your Arduino preference file. You can also manually set it through `ARDUINO_SKETCHBOOK`.
- `/libraries` directory inside your Arduino directory, which is read from `ARDUINO_DIR`.

The libraries inside user directories will take precedence over libraries present in Arduino core directory.

The makefile can autodetect the libraries that are included from your sketch and can include them automatically. But it can't detect libraries that are included from other libraries. (see [issue #93](https://github.com/sudar/Arduino-Makefile/issues/93))

## avrdude

To upload compiled files, `avrdude` is used. This Makefile tries to find `avrdude` and it's config (`avrdude.conf`) below `ARDUINO_DIR`. If you like to use the one installed on your system instead of the one which came with Arduino, you can try to set the variables `AVRDUDE` and `AVRDUDE_CONF`. On a typical Linux system these could be set to

```make
      AVRDUDE      = /usr/bin/avrdude
      AVRDUDE_CONF = /etc/avrdude.conf
```
